import {
  faBank,
  faLocationDot,
  faMoneyBill,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import Money from "../../assets/money.svg";
import Bank from "../../assets/bank.svg";

interface CardJobProps {
  item: any;
  index: any;
  currentCardJob: any;
  setCurrentCardJob: any;
}

const CardJob = ({
  item,
  index,
  currentCardJob,
  setCurrentCardJob,
}: CardJobProps) => {
  const arrSkill: string[] = item["skill_requirements"];

  const handleClick = () => {
    setCurrentCardJob(index);
  };

  return (
    <div className="flex flex-row mt-3" onClick={handleClick}>
      <div
        className={`border-2 rounded-2xl bg-white ${currentCardJob === index ? "bubble" : "border-stone-400"
          } w-11/12`}
      >
        <div>
          <p className="text-3xl font-semibold ml-5 mt-1">{item["job_name"]}</p>
        </div>
        <div className="flex flex-row pt-1 items-center">
          <img
            src={item["company"]["image"]}
            alt=""
            className={`rounded ${currentCardJob === index
                ? "shadow shadow-slate-500"
                : "border border-black"
              } w-10 h-12 ml-5 object-contain	`}
          />
          <p className="ml-3 text-xl font-medium">{item["company"]["name"]}</p>
        </div>
        <div className="mx-5 mt-3 mb-1">
          <p className="border border-t-black"></p>
        </div>
        <div
          className="flex flex-row pt-1 items-center ml-3"
          style={{ color: "#dc2626" }}
        >
          <FontAwesomeIcon icon={faLocationDot} className="ml-5 w-6 h-6" />
          <p className="text-xl pl-2">Cách {item["distance"]} km</p>
        </div>
        <div className="flex flex-row pt-1 items-center ml-3">
          {/* <FontAwesomeIcon icon={faBank} className="ml-5 w-6 h-6 font-thin" /> */}
          <img src={Bank} alt="bank" className="ml-5 w-6 h-6" />
          <p className="text-xl pl-2">{item["work_location"]}</p>
        </div>
        <div className="flex flex-row pt-1 items-center ml-3">
          {/* <FontAwesomeIcon icon={faMoneyBill} className="ml-5 w-6 h-6" /> */}
          <img src={Money} alt="money" className="ml-5 w-6 h-6 object-contain" />
          <p className="text-xl pl-2">
            {item["salary"].toLocaleString("en-US", {
              minimumFractionDigits: 0,
              maximumFractionDigits: 0,
            })}{" "}
            đ
          </p>
        </div>
        <div className="mx-5 mt-2 mb-1">
          <p className="border border-t-black"></p>
        </div>
        <div className="flex flex-row pt-1 items-center ml-5">
          {arrSkill.map((item: string, index: number) => {
            return (
              <div
                className="shadow-inner shadow-slate-500 rounded-full mb-5 ml-3"
                key={index}
              >
                <p className="text-xl px-2 py-1">{item}</p>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default CardJob;
