import Footer from "./footer";
import Header from "./header"
import { Outlet } from "react-router-dom";

const Layout = () => {
    return (
        <div className="flex h-screen flex-col bg-gray-100 transition-colors duration-150 overflow-x-hidden">
            <Header />
            <main className="flex flex-1 h-max">
                <div className="w-full overflow-y-auto">
                    <Outlet />
                </div>
            </main>
            <Footer />
        </div>
    )
}

export default Layout
