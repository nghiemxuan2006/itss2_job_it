import { useAppSelector, useAppDispatch } from "../../app/hook";
import React, { useState } from "react";
import "./result_search.css";
import Loading from "../loading";
import CardJob from "../card/CardJob";
import { useEffect } from "react";
import Money from "../../assets/money.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLocationDot } from "@fortawesome/free-solid-svg-icons";
import Bank from "../../assets/bank.svg";
import Alarm from "../../assets/alarm.svg";
import { Link, useNavigate } from "react-router-dom";
import {
  PayloadAction,
  unwrapResult,
  createAsyncThunk,
} from "@reduxjs/toolkit";
import apiService from "../../api";
import { Spin } from "antd";

interface CardJob {
  job_id: number;
}

interface Company {
  id: number;
  name: string;
  image: string;
  address: string;
  type: string;
  scale: string;
  nation: string;
  time_work: string;
  is_overtime: boolean;
}

interface JobDetail {
  job_id: number;
  job_name: string;
  company: Company;
  salary: number;
  work_location: string;
  type: string;
  skill_requirements: string[];
  description: string[];
}
const ResultSearch = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const status = useAppSelector((state) => state.searchJobList.status);
  const data = useAppSelector((state) => state.searchJobList.data) as CardJob[];
  const [currentCardJob, setCurrentCardJob] = useState<number>(0);
  const [loadingJobDetail, setLoadingJobDetail] = useState<boolean>(false);
  const [currentJobId, setCurrentJobId] = useState<number | null>();
  const [currentJobData, setCurrentJobData] = useState<JobDetail | null>();
  const [canApply, setCanApply] = useState<boolean>(false);

  useEffect(() => {
    if (data.length > 0) {
      onClickCardJob(0);
    } else {
      setCurrentJobData(null);
      setCurrentJobId(null);
    }
  }, [data]);

  const skillElements = [];
  const descriptionElements = [];

  for (let i = 0; i < currentJobData?.skill_requirements.length!; i++) {
    skillElements.push(
      <div key={i} className="skill_name py-1 px-3 ml-6">
        {currentJobData?.skill_requirements[i]}
      </div>
    );
  }
  for (let i = 0; i < currentJobData?.description.length!; i++) {
    descriptionElements.push(<li>{currentJobData?.description[i]}</li>);
  }

  const onClickApply = () => {
    navigate(
      `/sendCV?id=${currentJobData?.job_id}&company_name=${currentJobData?.company?.name}&job_name=${currentJobData?.job_name}`
    );
  };

  const onClickCardJob = async (index: number) => {
    setCurrentCardJob(index);
    setCurrentJobId(data[index].job_id);
    setLoadingJobDetail(true);
    const jobId = data[index].job_id;
    await dispatch(fetchJobApplyStatus({ jobId }))
      .then(unwrapResult)
      .then((res) => {
        if (res.applied) {
          setCanApply(false);
        } else {
          setCanApply(true);
        }
      });
    await dispatch(fetchJob({ jobId }))
      .then(unwrapResult)
      .then((res: JobDetail) => {
        setCurrentJobData(res);
      })
      .finally(() => {
        setLoadingJobDetail(false);
      });
  };

  const fetchJobApplyStatus = createAsyncThunk<any, any>(
    "job/applyStatus",
    async (payload) => {
      try {
        const response = await apiService.get(`/job/${payload.jobId}/applied`, {
          params: {},
        });

        return response.data;
      } catch (error) {
        console.error(error);
      }
    }
  );

  const fetchJob = createAsyncThunk<any, any>(
    "job/fetchJobDetail",
    async (payload) => {
      try {
        const response = await apiService.get(`/job/${payload.jobId}`, {
          params: {},
        });

        // Xử lý dữ liệu từ response
        const jobDetail = response.data;
        return jobDetail;
      } catch (error) {
        // Xử lý lỗi
        console.error(error);
      }
    }
  );

  if (status === "loading") {
    return (
      <div className="grid pt-12">
        <Loading isLoading={true} />
      </div>
    );
  } else if (status === "failed") {
    return <div>Failed to fetch data</div>;
  } else {
    let content = data.map((item, index) => {
      return (
        <CardJob
          key={index}
          item={item}
          index={index}
          currentCardJob={currentCardJob}
          setCurrentCardJob={onClickCardJob}
        />
      );
    });

    return (
      <div className="flex flex-row justify-center ml-14 mt-2 mb-2">
        {data.length > 0 ? (
          <React.Fragment>
            <div className="w-5/12">
              <div className="w-full">
                <p className="ms-5 text-2xl font-semibold ml-5">
                  {data.length} kết quả tìm kiếm phù hợp
                </p>
                <div className="scrollbar">
                  <div className="outer-container cursor-pointer">
                    {content}
                  </div>
                </div>
              </div>
            </div>
            <div className="w-5/12">
              {currentJobId !== null && (
                <>
                  <div className="job-detail-controller bg-white border-stone-400 w-full h-full p-2 border-2 rounded-xl">
                    {loadingJobDetail ? (
                      <div className="text-center pt-52">
                        <Spin size="large" />{" "}
                      </div>
                    ) : (
                      <>
                        <div className="grid grid-cols-6 gap-4 flex items-center">
                          <div className="col-span-1 job_img flex items-center">
                            <Link
                              to={`/detailCompany/${currentJobData?.company?.id}`}
                            >
                              <img
                                src={currentJobData?.company?.image}
                                alt=""
                                className="w-full object-contain	"
                              />
                            </Link>
                          </div>
                          <div className="col-span-5">
                            <div>
                              <strong>{currentJobData?.job_name}</strong>
                            </div>
                            <div>{currentJobData?.company?.name}</div>
                            <div className="flex items-center">
                              <img
                                src={Money}
                                alt="money"
                                className="w-6 h-6 d-flex item-center"
                              />
                              {currentJobData?.salary.toLocaleString("en-US", {
                                minimumFractionDigits: 0,
                                maximumFractionDigits: 0,
                              })}{" "}
                              đ
                            </div>
                          </div>
                        </div>
                        <div>
                          {canApply ? (
                            <button
                              onClick={onClickApply}
                              className="w-full my-4 py-2 text-white text-base bg-red-600 px-3 py-1 rounded-2xl bottom-0 right-0"
                            >
                              <span className="font-bold text-[20px]">
                                Ứng tuyển
                              </span>
                            </button>
                          ) : (
                            <button                              
                              className="w-full my-4 py-2 text-white text-base bg-[#292D32] px-3 py-1 rounded-2xl bottom-0 right-0 cursor-default"
                            >
                              <span className="font-bold text-[20px]">
                                Đã ứng tuyển
                              </span>
                            </button>
                          )}
                        </div>
                        <div>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="100%"
                            height="1"
                            viewBox="0 0 955 1"
                            fill="none"
                          >
                            <path d="M0 0.5H954.5" stroke="black" />
                          </svg>
                        </div>
                        <div className="company_address mt-1 flex items-center">
                          <FontAwesomeIcon
                            icon={faLocationDot}
                            className="ml-5 w-6 h-6"
                            style={{ color: "#dc2626" }}
                          />
                          <div className="text-[#F20909] ml-2">
                            <strong>{currentJobData?.company?.address}</strong>
                          </div>
                        </div>
                        <div className="company_address mt-1 flex items-center">
                          <img src={Bank} alt="bank" className="ml-5 w-6 h-6" />
                          <div className="ml-2">
                            {currentJobData?.work_location}
                          </div>
                        </div>
                        <div className="company_address mt-1 flex items-center">
                          <img
                            src={Alarm}
                            alt="alarm"
                            className="ml-5 w-6 h-6"
                          />
                          <div className="ml-2">{currentJobData?.type}</div>
                        </div>
                        <div className="skill flex items-center mb-3">
                          <div>
                            <strong>Kỹ năng:</strong>
                          </div>
                          {skillElements}
                        </div>
                        <div>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="100%"
                            height="1"
                            viewBox="0 0 955 1"
                            fill="none"
                          >
                            <path d="M0 0.5H954.5" stroke="black" />
                          </svg>
                        </div>
                        <div>
                          <strong>Mô tả công việc:</strong>
                          <ul>{descriptionElements}</ul>
                        </div>
                        <div className="mt-2">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="100%"
                            height="1"
                            viewBox="0 0 955 1"
                            fill="none"
                          >
                            <path d="M0 0.5H954.5" stroke="black" />
                          </svg>
                        </div>
                        <div>
                        <strong>{currentJobData?.company?.name}</strong>
                          {/* <strong>Goline corporation</strong> */}
                        </div>
                        <div className="grid grid-cols-3 gap-4">
                          <div className="col-span-1">
                            <div>
                              <div className="text-[#292D32] text-[14px]">
                                Mô hình công ty
                              </div>
                              <div className="">
                                {currentJobData?.company?.type}
                              </div>
                            </div>
                            <div className="mt-2">
                              <div className="text-[#292D32] text-[14px]">
                                Thời gian làm việc
                              </div>
                              <div className="">Thứ 2 - Thứ 6</div>
                            </div>
                          </div>
                          <div className="col-span-1">
                            <div>
                              <div className="text-[#292D32] text-[14px]">
                                Quy mô công ty
                              </div>
                              <div className="">
                                {currentJobData?.company?.scale}
                              </div>
                            </div>
                            <div className="mt-2">
                              <div className="text-[#292D32] text-[14px]">
                                Làm việc ngoài giờ
                              </div>
                              <div className="">
                                {currentJobData?.company?.is_overtime
                                  ? "Có OT"
                                  : "Không có OT"}
                              </div>
                            </div>
                          </div>
                          <div className="col-span-1">
                            <div>
                              <div className="text-[#292D32] text-[14px]">
                                Quốc gia
                              </div>
                              <div className="">
                                {currentJobData?.company?.nation}
                              </div>
                            </div>
                          </div>
                        </div>
                      </>
                    )}
                  </div>
                </>
              )}
            </div>
          </React.Fragment>
        ) : (
          <div className="bg-transparent flex flex-col items-center absolute top-1/3">
            <img
              className="w-1/4 mb-4"
              src="https://i.postimg.cc/GmgfV4CR/shiba-result.png"
              alt="No result"
            />
            <div>
              <strong>
                Xin lỗi! Việc làm bạn đang tìm kiếm không tồn tại.
              </strong>
            </div>
          </div>
        )}
        {/* <div className="w-5/12">
          <div className="w-full">
            <p className="ms-5 text-2xl font-semibold ml-5">
              {data.length} kết quả tìm kiếm phù hợp
            </p>
            <div className="scrollbar">
              <div className="outer-container cursor-pointer">{content}</div>
            </div>
          </div>
        </div>
        <div className="w-5/12">
          {currentJobId !== null && (
            <>
              <div className="job-detail-controller bg-white border-stone-400 w-full h-full p-2 border-2 rounded-xl">
                {loadingJobDetail ? (
                  <div className="text-center pt-52">
                    <Spin size="large" />{" "}
                  </div>
                ) : (
                  <>
                    <div className="grid grid-cols-6 gap-4 flex items-center">
                      <div className="col-span-1 job_img flex items-center">
                        <Link
                          to={`/detailCompany/${currentJobData?.company?.id}`}
                        >
                          <img
                            src={currentJobData?.company?.image}
                            alt=""
                            className="w-full"
                          />
                        </Link>
                      </div>
                      <div className="col-span-5">
                        <div>
                          <strong>{currentJobData?.job_name}</strong>
                        </div>
                        <div>{currentJobData?.company?.name}</div>
                        <div className="flex items-center">
                          <img
                            src={Money}
                            alt="money"
                            className="w-6 h-6 d-flex item-center"
                          />
                          {currentJobData?.salary.toLocaleString("en-US", {
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                          })}{" "}
                          đ
                        </div>
                      </div>
                    </div>
                    <div>
                      <button
                        onClick={onClickApply}
                        className="w-full my-4 py-2 text-white text-base bg-red-600 px-3 py-1 rounded-2xl bottom-0 right-0"
                      >
                        <span className="font-bold text-[20px]">Ứng tuyển</span>
                      </button>
                    </div>
                    <div>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="100%"
                        height="1"
                        viewBox="0 0 955 1"
                        fill="none"
                      >
                        <path d="M0 0.5H954.5" stroke="black" />
                      </svg>
                    </div>
                    <div className="company_address mt-1 flex items-center">
                      <FontAwesomeIcon
                        icon={faLocationDot}
                        className="ml-5 w-6 h-6"
                        style={{ color: "#dc2626" }}
                      />
                      <div className="text-[#F20909] ml-2">
                        <strong>{currentJobData?.company?.address}</strong>
                      </div>
                    </div>
                    <div className="company_address mt-1 flex items-center">
                      <img src={Bank} alt="bank" className="ml-5 w-6 h-6" />
                      <div className="ml-2">
                        {currentJobData?.work_location}
                      </div>
                    </div>
                    <div className="company_address mt-1 flex items-center">
                      <img src={Alarm} alt="alarm" className="ml-5 w-6 h-6" />
                      <div className="ml-2">{currentJobData?.type}</div>
                    </div>
                    <div className="skill flex items-center mb-3">
                      <div>
                        <strong>Kỹ năng:</strong>
                      </div>
                      {skillElements}
                    </div>
                    <div>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="100%"
                        height="1"
                        viewBox="0 0 955 1"
                        fill="none"
                      >
                        <path d="M0 0.5H954.5" stroke="black" />
                      </svg>
                    </div>
                    <div>
                      <strong>Mô tả công việc:</strong>
                      <ul>{descriptionElements}</ul>
                    </div>
                    <div className="mt-2">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="100%"
                        height="1"
                        viewBox="0 0 955 1"
                        fill="none"
                      >
                        <path d="M0 0.5H954.5" stroke="black" />
                      </svg>
                    </div>
                    <div>
                      <strong>Goline corporation</strong>
                    </div>
                    <div className="grid grid-cols-3 gap-4">
                      <div className="col-span-1">
                        <div>
                          <div className="text-[#292D32] text-[14px]">
                            Mô hình công ty
                          </div>
                          <div className="">
                            {currentJobData?.company?.type}
                          </div>
                        </div>
                        <div className="mt-2">
                          <div className="text-[#292D32] text-[14px]">
                            Thời gian làm việc
                          </div>
                          <div className="">Thứ 2 - Thứ 6</div>
                        </div>
                      </div>
                      <div className="col-span-1">
                        <div>
                          <div className="text-[#292D32] text-[14px]">
                            Quy mô công ty
                          </div>
                          <div className="">
                            {currentJobData?.company?.scale}
                          </div>
                        </div>
                        <div className="mt-2">
                          <div className="text-[#292D32] text-[14px]">
                            Làm việc ngoài giờ
                          </div>
                          <div className="">
                            {currentJobData?.company?.is_overtime
                              ? "Có OT"
                              : "Không có OT"}
                          </div>
                        </div>
                      </div>
                      <div className="col-span-1">
                        <div>
                          <div className="text-[#292D32] text-[14px]">
                            Quốc gia
                          </div>
                          <div className="">
                            {currentJobData?.company?.nation}
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </>
          )}
        </div> */}
      </div>
    );
  }
};

export default ResultSearch;
