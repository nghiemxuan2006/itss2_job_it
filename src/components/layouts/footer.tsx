import ImageHeader from '../../assets/image_header.svg'

const Footer = () => {
    return (
        <footer className="">
            <div className="flex flex-row justify-around bg-gradient-to-r from-fromColor to-toColor h-[150px]">
                <div className='flex flex-col items-center my-auto gap-y-3'>
                    <div className='flex flex-row gap-x-5'>
                        <i className="fa fa-brands fa-linkedin fa-2xs text-[#d5dae1]"></i>
                        <i className="fa fa-brands fa-facebook fa-2xs text-[#c5cedd]" ></i>
                        <i className="fa fa-brands fa-youtube fa-2xs text-[#d1d7e0]"></i>
                    </div>
                    <div>
                        <img src={ImageHeader} alt="abcd" />
                    </div>
                </div>
                <div className='text-white mt-3'>
                    Về INTERNSHIP
                </div>
                <div className='text-white mt-3'>
                    <p>Điều khoản chung</p>
                    <small>Quy định bảo mật</small>
                    <small className='block text-gray-200'>Quy chế hoạt động</small>
                    <small className='block text-gray-200'>Giải quyết khiếu nại</small>
                    <small className='block text-gray-200'>Thỏa thuận sử dụng</small>
                    <small className='block text-gray-200'>Thông cáo báo chí</small>
                </div>
                <div className='text-white mt-3'>
                    <p>Liên hệ để đăng tin tuyển dụng tại:</p>
                    <div className='flex flex-row items-center gap-x-3'>
                        <i className="fa fa-solid fa-phone text-[#d3d7df]"></i>
                        <small className='inline'>Hồ Chí Minh: +84: 977 460 519</small>
                    </div>
                    <div className='flex flex-row items-center gap-x-3'>
                        <i className="fa fa-solid fa-phone text-[#d3d7df]"></i>
                        <small className='inline'>Hà Nội: +84: 983 131 351</small>
                    </div>
                    <div className='flex flex-row items-center gap-x-2'>
                        <i className="fa fa-regular fa-paper-plane text-[#d7e2f4]"></i>
                        <small className='inline'>Gửi thông tin liên hệ</small>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer