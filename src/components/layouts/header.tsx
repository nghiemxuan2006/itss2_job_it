import { useAppSelector, useAppDispatch } from '../../app/hook';
import ImageHeader from '../../assets/image_header.svg';
import Bell from '../../assets/bell-regular.svg';
import LinhDinh from '../../assets/linhdinh.svg';
import { DownOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Dropdown, Space } from 'antd';
import { Popover } from 'antd';
import { useEffect, useState } from 'react';
import { createAsyncThunk, unwrapResult } from '@reduxjs/toolkit';
import apiService from '../../api';

interface Notice {
    notification_id: number;
    job_id: number;
    company_id: number;
    company_name: string;
    apply_time: string;
}

const items: MenuProps['items'] = [
    {
        label: <a href="#">Hồ sơ của tôi</a>,
        key: '0',
    },
    {
        label: <a href="#">Đăng xuất</a>,
        key: '1',
    },
    // {
    //     type: 'divider',
    // },
    // {
    //     label: '3rd menu item（disabled）',
    //     key: '3',
    //     disabled: true,
    // },
];
const Header = () => {
    const dispatch = useAppDispatch();

    //   const [notices, setNotices] = useState([
    //     "Công ty Goline corporation",
    //     "Công ty FueCloud",
    //   ]);
    const [notices, setNotices] = useState<Notice[]>([]);

    //   const onClickCardJob = (index: number) => {
    //     dispatch(fetchNotification({ }))
    //       .then(unwrapResult)
    //       .then((res: Notice[]) => {
    //         setNotices(res);
    //       });
    //   };
    useEffect(() => {
        // Dispatch the fetchNotification action when the component mounts
        dispatch(
            fetchNotification({
                /* your payload object */
            }),
        );
    }, [dispatch]);

    const fetchNotification = createAsyncThunk<any, any>('job/notification', async (payload) => {
        try {
            const response = await apiService.get(`/notification`, {
                params: payload,
            });

            // Xử lý dữ liệu từ response
            const notification = response.data;
            setNotices(response.data);
            return notification;
        } catch (error) {
            // Xử lý lỗi
            console.error(error);
        }
    });
    const content = (
        <div className="w-[400px] h-[500px] overflow-y-scroll">
            <div className="text-lg font-bold border-b-2 border-stone-400">Thông báo</div>
            {notices && notices.length > 0 ? (
                notices?.map((notice) => {
                    return (
                        <div className="border-b-2 border-stone-400 my-2">
                            <p className="font-semibold">Ứng tuyển thành công</p>
                            <p className="font-medium">{notice.company_name}</p>
                            <p>Chúng tôi đã gửi CV đến doanh nghiệp</p>
                            <p className="text-rose-600">
                                Vui lòng chờ phản hồi phía doanh nghiệp qua gmail đã đăng kí
                            </p>
                        </div>
                    );
                })
            ) : (
                <p className="mt-2">Không có thông báo</p>
            )}
        </div>
    );
    return (
        <header className="">
            <div className="h-full flex flex-row justify-between bg-gradient-to-r from-fromColor to-toColor">
                <a href="/">
                    <div className="ms-12 flex flex-row items-center">
                        <p className="font-normal text-5xl font-modak align-middle text-white ">INTERNSHIP</p>
                        <img src={ImageHeader} alt="abcd" />
                    </div>
                </a>
                <div className="flex flex-row items-center gap-x-3 me-5">
                    <img src={LinhDinh} alt="avatar"></img>
                    <Dropdown menu={{ items }}>
                        <a onClick={(e) => e.preventDefault()}>
                            <Space className="text-white font-rasa font-medium cursor-pointer">
                                Linh Dinh xinh gai
                                <DownOutlined />
                            </Space>
                        </a>
                    </Dropdown>
                    <Popover content={content} placement="bottomRight">
                        <img src={Bell} alt="" className=" h-[25px] cursor-pointer pe-2" />
                    </Popover>
                </div>
            </div>
        </header>
    );
};

export default Header;
