import img_1 from '../../assets/img_1.svg';
import img_2 from '../../assets/img_2.svg';
import img_3 from '../../assets/img_3.svg';
import Footer from './footer';

const BodyIndex = () => {
    return (
        <div className="flex flex-col my-7 px-8">
            <div className="mb-11">
                <h3 className="text-center text-2xl font-medium">
                    Công cụ tốt nhất cho hành trang ứng tuyển của bạn
                </h3>
            </div>
            <div className="flex flex-row justify-between items-center">
                <div className="w-3/12">
                    <img src={img_1} alt="" className="text-center w-1/3 mx-auto mb-3" />
                    <p className="text-center text-base">
                        Danh sách việc làm "chất" liên tục cập nhật các lựa chọn mới nhất theo thị trường và xu
                        hướng tìm kiếm.
                    </p>
                </div>
                <div className="w-3/12">
                    <img src={img_2} alt="" className="text-center w-1/3 mx-auto mb-3" />
                    <p className="text-center text-base">
                        Kiến tạo hồ sơ với bố cục chuẩn mực, chuyên nghiệp dành riêng cho từng ngành, được nhiều nhà
                        tuyển dụng đề xuất.
                    </p>
                </div>
                <div className="w-3/12">
                    <img src={img_3} alt="" className="text-center w-1/3 mx-auto mb-3" />
                    <p className="text-center text-base">
                        Đừng bỏ lỡ cơ hội cập nhật thông tin lương thưởng, chế độ làm việc, nghề nghiệp và kiến thức
                    </p>
                </div>
            </div>
        </div>
    );
};

export default BodyIndex;
