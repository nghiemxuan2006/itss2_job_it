import './apply_cv.css';
import ApplyCvBody from '../apply-cv/apply-cv-body';
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import { useState, useRef, RefObject, useEffect } from 'react';
import ApplySuccessBody from '../apply-cv/apply-success';
import { showNotifications } from '../../utils';

interface ChildComponentRef {
    onClickBack: () => void;
    // Add other methods or properties if needed
}

const ApplyCv = () => {
    // const childRef = useRef();
    const childRef: RefObject<ChildComponentRef> = useRef(null);

    const [isSuccess, setIsSuccess] = useState<boolean>(false);
    const [searchParams, setSearchParams] = useSearchParams();
    const navigate = useNavigate();

    // useEffect(() => {
    //   window.onbeforeunload = (event) => {
    //     const e = event || window.event;
    //     // Cancel the event
    //     e.preventDefault();
    //     if (e) {
    //       e.returnValue = 'ndsablobiasdias'; // Legacy method for cross browser support
    //     }
    //     return 'hello'; // Legacy method for cross browser support
    //   };
    //   // window.history.pushState(null, null, document.URL);
    //   window.onpopstate = (event) => {
    //     event.preventDefault()
    //     const isOk = window.confirm('Are you sure?')
    //     // if (!isOk) window.history.go(0);
    //     return true;
    //   }
    // }, []);

    const location = useLocation();

    useEffect(() => {
        const handleBeforeRouterChange = () => {
            // Perform actions before the router changes
            alert('abcd');
            console.log('Performing actions before router change...');
        };

        const handleBeforeUnload = (event: any) => {
            // Cancel the default behavior of the event to prevent the page from unloading
            // event.preventDefault();
            event.returnValue = true;

            // // Opening the confirmation dialog
            // const confirmationMessage = 'Are you sure you want to leave this page?';
            // event.returnValue = confirmationMessage; // For older browsers
            // return confirmationMessage; // For modern browsers
        };

        // const handlePopstate = (event: any) => {

        //   event.stopImmediatePropagation()
        //   const isOk = window.confirm('Are you sure?' + location.pathname)
        //   if (!isOk) {
        //     navigate(location.pathname); // Navigate to the new route
        //   }
        // };

        window.addEventListener('beforeunload', handleBeforeUnload);
        // window.onpopstate = handlePopstate

        return () => {
            // window.removeEventListener('popstate', handlePopstate);
            window.removeEventListener('beforeunload', handleBeforeUnload);
        };
    }, [navigate]);

    const onClickGoBack = () => {
        //
        if (childRef && childRef.current) {
            childRef.current.onClickBack();
        } else {
            console.error('childRef is undefined or null');
        }
    };

    const onSubmitSuccess = (status: boolean) => {
        setIsSuccess(true);
    };
    return (
        <div className="apply-cv-container">
            <div className="apply-cv-main py-10 mx-auto text-center">
                <div className="apply-cv-header text-white relative">
                    {!isSuccess ? (
                        <div className="action-return bg-transparent absolute bottom-2">
                            <i className="fa-solid fa-chevron-left"></i>
                            <span className="px-3 cursor-pointer" onClick={() => onClickGoBack()}>
                                Quay lại
                            </span>
                        </div>
                    ) : (
                        ''
                    )}
                    <p className="font-normal text-5xl font-modak align-middle text-white">INTERNSHIP</p>
                </div>
                <div className="apply-cv-body bg-white border-2 mt-3 border-black rounded-lg">
                    {!isSuccess ? (
                        <ApplyCvBody
                            ref={childRef}
                            onSubmitSuccess={onSubmitSuccess}
                            companyName={searchParams.get('company_name')!}
                            jobName={searchParams.get('job_name')!}
                            jobId={searchParams.get('id')!}
                        />
                    ) : (
                        <ApplySuccessBody
                            companyName={searchParams.get('company_name')!}
                            jobName={searchParams.get('job_name')!}
                            jobId={searchParams.get('id')!}
                        />
                    )}
                </div>
            </div>
        </div>
    );
};

export default ApplyCv;
