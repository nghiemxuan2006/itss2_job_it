import {
  Button,
  Form,
  Input,
  Radio,
  Spin,
  Upload,
  UploadFile,
} from "antd";
import "./apply-cv-body.css";
import { RcFile } from "antd/lib/upload/interface";
import { ReactNode, useEffect, useMemo, useRef, useState } from "react";
import React from "react";
import apiService from "../../api";
import { toast } from "react-toastify";
import { showNotifications } from "../../utils";
import { useNavigate } from "react-router-dom";
import { UploadChangeParam, UploadProps } from "antd/es/upload/interface";
import { StarOutlined } from "@ant-design/icons";

const { TextArea } = Input;

interface DetailJob {
  companyName: string;
  jobName: string;
  jobId: string;
  onSubmitSuccess: (status: boolean) => void;
}

const ApplyCvBody = React.forwardRef(({ companyName, jobName, jobId, onSubmitSuccess }: DetailJob, ref) => {
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isApplied, setIsApplied] = useState<boolean>(false);
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [isSuccess, setIsSuccess] = useState(false);
  // const fileListRef = useRef<UploadFile[]>([]);
  const [currentCV, setCurrentCV] = useState<UploadFile>({
    uid: "1",
    name: "",
    status: "done",
    url: "",
  });

  const onClickBack = () => {
    const currentFormValue = form.getFieldsValue();
    if (currentFormValue.name || currentFormValue.intro_letter || currentFormValue.use_current_cv) {
      const isOk = window.confirm("Are you sure?")
      if (isOk) navigate('/')
    } else {
      navigate('/');
    }
  };

  React.useImperativeHandle(ref, () => ({
    onClickBack
  }));

  const [form] = Form.useForm<{
    name: string;
    use_current_cv: string;
    intro_letter: string;
  }>();
  const use_current_cv = Form.useWatch("use_current_cv", form);
  const intro_letter: string = Form.useWatch("intro_letter", form);

  const onFinish = async (values: any) => {
    const currentFormValue = form.getFieldsValue();
    if (!use_current_cv) {
      showNotifications("error", "Chọn phương pháp chọn cv ứng tuyển");
    } else if (
      use_current_cv === "no" &&
      (!fileList[0] || fileList.length === 0)
    ) {
      showNotifications("error", "Vui lòng tải lên cv của bạn");
    } else {
      const send_form = new FormData();
      send_form.append("name", currentFormValue.name);
      send_form.append("file", fileList[0]?.originFileObj as Blob);
      send_form.append("use_current_cv", currentFormValue.use_current_cv);
      send_form.append("intro_letter", currentFormValue.intro_letter ?? "");
      form
        .validateFields()
        .then(async (values) => {
          setIsLoading(true);
          const response = await apiService
            .post("/job/" + jobId + "/upload_cv", send_form)
            .then((res) => {
              if (res && res.status === 200) {
                // showNotifications("success", "Upload cv thành công!!!");
                fetchCurrentCV();
                onSubmitSuccess(true);
              }
            })
            .catch((error) => {
              showNotifications("error", "Có lỗi xảy ra!!!");
            })
            .finally(() => {
              setIsLoading(false);
            });
        })
        .catch((errorInfo) => { });
    }
  };

  const beforeUpload = async (file: RcFile) => {
    const isPDF = file.type === "application/pdf";
    if (!isPDF) {
      showNotifications("error", "You can only upload PDF files!");
      setFileList([]);
      setIsSuccess(false);
    } else if (file.size > 3000000) {
      setFileList([]);
      showNotifications("error", "File size is more than 3MB!");
      setIsSuccess(false);
    } else {
      showNotifications("success", "Upload cv thành công!");
      setIsSuccess(true);
    }
    return false;
  };

  const onChange = (info: UploadChangeParam<UploadFile>) => {
    if (info.file && info.file.type !== "application/pdf") {
      return;
    }
    if (info.file.size && info.file.size > 3000000) {
      return;
    }
    if (info.file && info.file.type === "application/pdf") {
      setFileList(info.fileList);
    }
  };

  // const props = {
  //   multiple: false,
  //   fileList: fileListRef.current,
  //   beforeUpload: (file: RcFile) => {
  //     const isPDF = file.type === "application/pdf";
  //       debugger
  //       if (!isPDF) {
  //       showNotifications('error', "You can only upload PDF files!");
  //       fileListRef.current = [{
  //         uid: "1",
  //         name: "đasa",
  //         status: "done",
  //         url: "",
  //       }];
  //     } else if (file.size > 300000) {
  //       showNotifications('error', "File size is more than 3MB!");
  //       fileListRef.current = [{
  //         uid: "1",
  //         name: "uck",
  //         status: "done",
  //         url: "",
  //       }];
  //     }else{
  //       showNotifications('success', "Upload cv thành công!");
  //     }
  //     return false;
  //   },
  //   maxCount: 1,
  //   onChange : (info: any)  =>{
  //     if (
  //       info.file &&
  //       info.file.type === "application/pdf" &&
  //       info.file.status !== "uploading"
  //     ) {
  //       fileListRef.current = info.fileList;
  //     }
  //   },
  //   // action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  //   name: "file",
  // };

  // console.log({props})

  const fetchCurrentCV = async () => {
    try {
      setIsLoading(true);
      const response = await apiService.get("/user/cv");
      const data = response.data;
      if (data && data.cv_link && data.cv_name) {
        setCurrentCV({
          uid: "1",
          name: data.cv_name,
          status: "done",
          url: data.cv_link,
        });
      } else {
        setCurrentCV({
          uid: "1",
          name: "",
          status: "done",
          url: "",
        });
      }
    } catch (error) {
      // Xử lý lỗi
      showNotifications(
        "error",
        "Có lỗi xảy ra, không load được cv đã upload!"
      );
      console.error(error);
    }
    setIsLoading(false);


  };

  const fetchApplyHistory = async () => {
    try {
      setIsLoading(true);
      const response = await apiService.get("/job/" + jobId + '/applied');
      const data = response.data;
      if (data) {
        setIsApplied(data.applied);
      }
    } catch (error) {
      console.error(error);
    }
    setIsLoading(false);

  };

  useEffect(() => {
    fetchCurrentCV();
    fetchApplyHistory()
  }, []);

  return (
    <div className="px-24 pt-4 text-left">
      <div className="apply-cv-job-name py-2">
        <span className="text-3xl font-bold text-black">
          {jobName} tại {companyName}
        </span>
      </div>
      <Form
        form={form}
        name="applyCvForm"
        layout="vertical"
        autoComplete="off"
      >
        <div className="form-label font-bold py-1">
          Tên<span className="text-red-500 ml-0.5">*</span>
        </div>
        <Form.Item
          className="mb-2"
          name="name"
          labelAlign="left"
          rules={[{ required: true, message: "" }]}
        >
          <Input
            className="py-2"
            placeholder="Nhập họ tên"
          />
        </Form.Item>

        <div className="form-label font-bold py-1">
          CV ứng tuyển<span className="text-red-500 ml-0.5">*</span>
        </div>
        <Form.Item
          className="mb-2"
          name="use_current_cv"
          rules={[{ required: true, message: "" }]}
        >
          <Radio.Group className="w-full">
            {currentCV.name ? (
              <div
                className={`${use_current_cv === "yes"
                  ? "color-selected"
                  : ""
                  } cv-option border-2 border-gray-200 rounded-lg p-3 my-2`}
              >
                <Radio
                  disabled={currentCV.name ? false : true}
                  value={"yes"}
                >
                  Sử dụng cv đã upload
                </Radio>
                <Upload defaultFileList={[currentCV] || []}></Upload>
              </div>
            ) : (
              ""
            )}
            <div
              className={`${use_current_cv === "no" ? "color-selected" : ""
                } cv-option border-2 border-gray-200 rounded-lg p-3 my-2`}
            // style={{background: 'rgba(255, 9, 9, 0.2)'}}

            >
              <Radio value={"no"}>Tải lên cv mới</Radio>
              <Upload
                name="file"
                beforeUpload={beforeUpload}
                onChange={onChange}
                maxCount={1}
                fileList={fileList}
                multiple={false}
              // showUploadList={isSuccess}
              // onChange={(response) => {
              //   if (response.file.status !== "uploading") {
              //     console.log(response.file, response.fileList);
              //   }
              //   if (response.file.status === "done") {
              //     message.success(`${response.file.name}
              //                file uploaded successfully`);
              //   } else if (response.file.status === "error") {
              //     message.error(`${response.file.name}
              //              file upload failed.`);
              //   }
              // }}
              >
                {use_current_cv === "no" ? (
                  <div>
                    <Button
                      type="default"
                      size="small"
                      style={{
                        fontSize: "0.75rem",
                        padding: "0 15px",
                      }}
                      className={`${fileList.length > 0 ? "" : ""
                        } bg-gray-400 mt-2 ml-5 mt-5 px-5 text-sm border color-black border-gray-500 hover:border-red-500 hover:color-red `}
                    >
                      Chọn File
                    </Button>
                    <span className="text-xs ml-5 mt-1 text-gray-700 block">
                      Hỗ trợ định dạng .doc .docx .pdf, không chứa mật khẩu bảo
                      vệ, dung lượng dưới 3MB
                    </span>
                  </div>
                ) : (
                  ""
                )}
              </Upload>
            </div>
          </Radio.Group>
        </Form.Item>

        <div className="form-label font-bold py-1">
          Thư xin thực tập
          <span className="text-stone-300 text-sm font-normal ml-3">
            (Không bắt buộc)
          </span>
        </div>
        <div className="form-addtional py-1 pb-3">
          <span>
            Những kỹ năng, dự án hay thành tựu nào chứng tỏ bạn là một ứng viên
            tiềm năng cho vị trí ứng tuyển này?
          </span>
        </div>
        <Form.Item
          name="intro_letter"
          className="p-0 m-0 mb-2"
        >
          <TextArea
            rows={4}
            maxLength={100}
            placeholder="Nêu nhiều ví dụ cụ thể để làm hồ sơ ứng tuyển của bạn thuyết phục hơn..."
          />
        </Form.Item>
        <div className="text-sm text-gray-500 mb-5">
          <span>
            Còn lại {500 - (intro_letter ? intro_letter.length : 0)} ký tự trong
            tổng số 500 ký tự
          </span>
        </div>

        <Button
          className="w-full h-auto my-5"
          style={{
            fontSize: "1.2rem",
            padding: "0.7rem",
            lineHeight: "1.5rem",
            height: "auto",
          }}
          disabled={isApplied || isLoading}
          danger={true}
          size="large"
          type="primary"
          htmlType="submit"
          onClick={onFinish}
        >
          {isLoading ? <Spin className="mr-1 text-white" /> : isApplied ? 'Đã ứng tuyển' : 'Gửi CV của tôi'}
        </Button>
      </Form>
    </div>
  );
});

export default ApplyCvBody;
