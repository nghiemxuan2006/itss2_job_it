import { Button } from "antd";
import { useNavigate } from "react-router-dom";
import img1 from "../../assets/success_logo1.png"
import img2 from "../../assets/success_logo2.png"
import { useAppDispatch } from "../../app/hook";
import { setData } from "../../features/search/searchJobList";
import { clickBtnSearch, setFilterData, setIsClickBtnSearch, setIsSendCVSuccess } from "../../features/search/btnSearchSlice";

interface DetailJob {
  companyName: string;
  jobName: string;
  jobId: string;
}

const ApplySuccessBody = ({ companyName, jobName, jobId }: DetailJob) => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate();
  const onClickBack = async () => {
    // await dispatch(setData([]))
    await dispatch(setIsSendCVSuccess(true))
    // await dispatch(setFilterData({
    //   name: '',
    //   salary_from: null,
    //   salary_to: null,
    //   distance_from: null,
    //   distance_to: null,
    //   type: '',
    //   work_location: '',
    // }))
    // await dispatch(setIsClickBtnSearch(false))
    navigate(`/`);
  }
  return (
    <div className="p-6">
      <div className="logo flex justify-center">
        <div className="logo1">
          <img src={img2} width={100} height={100}></img>
        </div>
        <div className="logo2">
          <img src={img1} width={100} height={100}></img>

        </div>
      </div>

      <div className="text-center my-3">
        <span className="font-semibold">
          Tuyệt vời! CV của bạn đã được ghi nhận
        </span>
      </div>

      <div className="text-left">
        <span className="block">
          Chúng tôi đã nhận được CV của bạn cho:
        </span>

        <ul>
          <li>
            Vị trí: {companyName}
          </li>
          <li>
            Công ty: {jobName}
          </li>
        </ul>

        <span className="block mt-5">
          CV của bạn sẽ được gửi tới nhà tuyển dụng sau khi
          được INTERNSHIP xét duyệt.
        </span>
        <span className="block text-red-500 mt-5"> Vui lòng theo dõi qua thông báo và gmail đã
          đăng ký để cập nhật thông tin về tình trạng CV.</span>

      </div>

      <div className="action back">
        <Button
          className="w-full h-auto my-5"
          style={{
            fontSize: "1.2rem",
            padding: "0.7rem",
            lineHeight: "1.5rem",
            height: "auto",
          }}
          danger={true}
          size="large"
          type="primary"
          htmlType="button"
          onClick={onClickBack}
        >
          Trở về
        </Button>
      </div>
    </div>
  );
};

export default ApplySuccessBody;
