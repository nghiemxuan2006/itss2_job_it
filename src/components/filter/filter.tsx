import { faCompass, faLocation, faLocationDot } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DownOutlined, SearchOutlined } from '@ant-design/icons';
import { Dropdown, MenuProps, Modal, Select, Space } from 'antd';
import { Outlet } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../app/hook';
import { clickBtnSearch, setFilterData } from '../../features/search/btnSearchSlice';
import { fetchJob } from '../../features/search/searchJobList';
import { useEffect, useState } from 'react';
import { showNotifications } from '../../utils';

const { Option } = Select;

interface FormData {
    name: string;
    salary_from: number | null;
    salary_to: number | null;
    distance_from: number | null;
    distance_to: number | null;
    type: string;
    work_location: string;
}

const Filter = () => {
    const dispatch = useAppDispatch();
    const filterData = useAppSelector((state) => state.btnSearch.filterData);
    const isSendCVSuccess = useAppSelector((state) => state.btnSearch.isSendCVSuccess);

    //Lấy dữ liệu form
    const [formData, setFormData] = useState<FormData>({
        name: '',
        salary_from: null,
        salary_to: null,
        distance_from: null,
        distance_to: null,
        type: '',
        work_location: '',
    });

    useEffect(() => {
        if (isSendCVSuccess) {
            setFormData(filterData);
        }
    }, [isSendCVSuccess, filterData]);
    // Lấy dữ liệu Input
    const handleChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormData((prevFormData) => ({
            ...prevFormData,
            [name]: value,
        }));
    };
    // Lấy dữ liệu Select
    const handleChangeSelectNumberType = (nameOne: string, nameTwo: string, value: number) => {
        if (nameOne === 'salary_from') {
            setFormData((prevFormData) => ({
                ...prevFormData,
                [nameOne]: value,
                [nameTwo]: value + 1000000,
            }));
        } else {
            setFormData((prevFormData) => ({
                ...prevFormData,
                [nameOne]: value,
                [nameTwo]: value + 1,
            }));
        }
    };

    const handleChangeSelectStringType = (name: string, value: string) => {
        setFormData((prevFormData) => ({
            ...prevFormData,
            [name]: value,
        }));
    };

    // Call Api search
    const handleSearch = () => {
        console.log('Form Data:', formData);
        let checkFormData = true;
        let textNotification;
        if (formData.name === '' || formData.distance_from === null) {
            checkFormData = false;
            if (formData.name === '' && formData.distance_from != null) {
                textNotification = 'Thiếu tên công ty hoặc tên công việc';
            } else if (formData.name !== '' && formData.distance_from === null) {
                textNotification = 'Thiếu trường khoảng cách';
            } else {
                textNotification = 'Thiếu cả 2 trường khoảng cách và tên công ty, tên công việc';
            }
        }
        console.log('form: ', formData);
        console.log('text: ', textNotification);
        if (checkFormData === false) {
            showNotifications('warning', textNotification);
        } else {
            dispatch(setFilterData(formData));
            dispatch(clickBtnSearch());
            dispatch(fetchJob(formData));
        }
    };
    return (
        <div className="bg-gradient-to-r from-fromColor to-toColor py-6 border-none" style={{ marginTop: '0.8px' }}>
            <div className="flex flex-row justify-center mx-12">
                <div className="border-none mr-3">
                    <p className="bg-white px-2 py-1.5 rounded-2xl">
                        <FontAwesomeIcon icon={faLocationDot} className="pr-2" />
                        <span className="font-medium pr-2">HUST</span>
                    </p>
                </div>
                <form className="flex flex-row justify-between w-9/12">
                    <div className="flex flex-col justify-between w-9/12 mr-2">
                        <input
                            className="w-full mb-3 py-1 px-3 rounded"
                            placeholder="Nhập từ khóa kỹ năng, tên công ty..."
                            name="name"
                            onChange={handleChangeInput}
                            value={formData.name}
                        ></input>
                        <div className="flex flex-row justify-between ">
                            <Select
                                placeholder="Khoảng cách"
                                value={formData.distance_from}
                                className="w-1/2 mr-2"
                                onSelect={(value: number) =>
                                    handleChangeSelectNumberType('distance_from', 'distance_to', value)
                                }
                                options={[
                                    { value: 0, label: 'Dưới 1km' },
                                    { value: 1, label: 'Từ 1km-2km' },
                                    { value: 2, label: 'Từ 2km-3km' },
                                    { value: 3, label: 'Từ 3km-4km' },
                                    { value: 4, label: 'Từ 4km -5km' },
                                    { value: 5, label: 'Trên 5km ' },
                                ]}
                            />
                            <Select
                                placeholder="Hình thức làm việc"
                                value={formData.work_location || null}
                                className="w-1/2"
                                onSelect={(value: string) => handleChangeSelectStringType('work_location', value)}
                                options={[
                                    { value: 'office', label: 'Văn phòng' },
                                    { value: 'online', label: 'OnLine' },
                                    { value: 'both', label: 'Cả Hai' },
                                ]}
                            />
                        </div>
                    </div>
                    <div className="flex flex-col justify-between w-3/12">
                        <div className="w-full">
                            <Select
                                placeholder="Mức lương"
                                value={formData.salary_from}
                                className="w-full"
                                onSelect={(value: number) =>
                                    handleChangeSelectNumberType('salary_from', 'salary_to', value)
                                }
                                options={[
                                    { value: 0, label: 'Từ 0 - 1.000.000.đ' },
                                    { value: 1000000, label: 'Từ 1.000.000 - 2.000.000 đ' },
                                    { value: 2000000, label: 'từ 2.000.000 - 3.000.000 đ' },
                                    { value: 4000000, label: 'Từ 4.000.000 - 5.000.000 đ' },
                                    { value: 5000000, label: 'Trên 5.000.000 đ' },
                                ]}
                            />
                        </div>
                        <div>
                            <Select
                                placeholder="Thời gian"
                                value={formData.type || null}
                                className="w-full"
                                onSelect={(value: string) => handleChangeSelectStringType('type', value)}
                                options={[
                                    { value: 'part time', label: 'Part-time' },
                                    { value: 'full time', label: 'Full-time' },
                                ]}
                            />
                        </div>
                    </div>
                </form>
                <div className="flex flex-row items-end ml-3">
                    <button
                        className="text-white text-base bg-red-600 px-3 py-1 rounded-2xl bottom-0 right-0"
                        onClick={handleSearch}
                    >
                        <SearchOutlined className="mr-3" />
                        <span className="font-normal mr-1">Tìm kiếm</span>
                    </button>
                </div>
            </div>
            <Outlet />
        </div>
    );
};

export default Filter;
