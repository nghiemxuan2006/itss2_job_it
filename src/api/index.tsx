import axios from "axios"
// const baseUrl = process.env.REACT_APP_BASE_URL
const baseUrl = 'https://shiba-internship.azurewebsites.net/api/v1';
const apiService = axios.create({
    baseURL: baseUrl,
    headers: {
        Accept: 'application/json',
    }
})


const access_token = localStorage.getItem('access_token') || 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJEdWNIdXkiLCJlbWFpbCI6Imh1eUBnbWFpbC5jb20iLCJhdmF0YXIiOiJodHRwczovL3l0My5nb29nbGV1c2VyY29udGVudC5jb20vLUNGVEpIVTdmRVdiN0JZRWI2Smg5Z20xRXBldHZWR1FxdG9mMFJiaC1WUVJJem5ZWUtKeENhcXZfOUhlQmNtSm1Jc3Aydk9POUpVPXM5MDAtYy1rLWMweDAwZmZmZmZmLW5vLXJqIiwiaWF0IjoxNzAyOTI5ODQwLCJleHAiOjE3MTE1Njk4NDB9.ybp-asGd14mgRbrLEmiUSg7mbrqZgSfcCz9jFveFusQ';
const refresh_token = localStorage.getItem('refresh_token')
apiService.interceptors.request.use((config) => {
    // Do something before request is sent
    if (config.headers) {
        config.headers.Authorization = `Bearer ${access_token}`
    }
    return config
})

apiService.interceptors.response.use((response) => {
    // Do something with response data

    return response
}, (error) => {
    // Do something with response error
    // refresh token when accesstoken is expired
    const status = error.response?.status || 500;
    switch (status) {
        // authentication
        case 401:
            break;
        //forbidden
        case 403:
            break;
        //bad request
        case 400:
            break;
        //not found
        case 404:
            break;
        default:
            break;
    }
    return Promise.reject(error)
})

export default apiService