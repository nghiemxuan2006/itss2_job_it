import React from 'react';
import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import NotFoundPage from './pages/404';
import SearchPage from './pages/search/search';
import PrivateRoute from './components/commons/PrivateRoute';
import Login from './pages/login/login';
// import Header from './components/layouts/header';
import SendCV from './pages/sendCV';
import Layout from './components/layouts';
import Home from './pages/home/home';
import ApplyCv from './components/layouts/apply_cv';
import DetailCompany from './pages/detailCompany/detail_company';

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/login" element={<Login />} />
                <Route path="" element={<Layout />}>
                    <Route path="/" element={<Home />} />
                    <Route path="/search" element={<SearchPage />} /> {/* 👈 Renders at /app/ */}
                    <Route path="/detailCompany/:id" element={<DetailCompany />} />
                </Route>
                <Route element={<PrivateRoute />}>
                    <Route path="/sendCV" element={<ApplyCv />} />
                </Route>
                <Route path="/*" element={<NotFoundPage />} />
            </Routes>
            <ToastContainer />
        </BrowserRouter>
    );
}

export default App;
