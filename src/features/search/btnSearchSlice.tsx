import { createSlice } from "@reduxjs/toolkit";

const btnSearchSlice = createSlice({
    name: 'btnSearch',
    initialState: {
        isClickedBtnSearch: false,
        filterData: {
            name: '',
            salary_from: null,
            salary_to: null,
            distance_from: null,
            distance_to: null,
            type: '',
            work_location: '',
        },
        isSendCVSuccess: true
    },
    reducers: {
        clickBtnSearch: (state) => {
            state.isClickedBtnSearch = true
        },
        setFilterData: (state, action) => {
            state.filterData = action.payload
        },
        setIsSendCVSuccess: (state, action) => {
            state.isSendCVSuccess = action.payload
        },
        setIsClickBtnSearch: (state, action) => {
            state.isClickedBtnSearch = action.payload
        }
    }
});

export default btnSearchSlice.reducer
export const { clickBtnSearch, setFilterData, setIsSendCVSuccess, setIsClickBtnSearch } = btnSearchSlice.actions