import axios from 'axios';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { sendRequest } from '../../utils';
import apiService from '../../api';

const initialState = {
    data: [],
    status: 'idle',
};

export const fetchJob = createAsyncThunk<any, any>('job/fetchJob', async (payload) => {
    try {
        const response = await apiService.get('/job', {
            params: payload
        });

        // Xử lý dữ liệu từ response
        const data = response.data;
        console.log(data);
        return data;
    } catch (error) {
        // Xử lý lỗi
        console.error(error);
    }
});

const searchJobListSlice = createSlice({
    name: 'searchJobList',
    initialState,
    reducers: {
        setData: (state, action) => {
            state.data = action.payload
        }
    },
    extraReducers(builder) {
        builder
            .addCase(fetchJob.pending, (state, action) => {
                state.status = 'loading';
            })
            .addCase(fetchJob.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.data = action.payload;
            })
            .addCase(fetchJob.rejected, (state, action) => {
                state.status = 'failed';
            });
    },
});

export default searchJobListSlice.reducer;
export const { setData } = searchJobListSlice.actions
