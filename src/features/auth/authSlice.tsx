import { createSlice } from "@reduxjs/toolkit";
import { loginRequest } from "./authApi";
interface userType {
    username: string,
    id: number,
    email: string
}

const initialState: { user: userType, isAuthenticated: boolean } = {
    user: {
        username: "linhxinh",
        email: "linhxinh@gmail.com",
        id: 1,
    },
    isAuthenticated: true, // fake
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {

    },
    extraReducers: (builder) => {
        builder.addCase(loginRequest.fulfilled, (state, action) => {
            console.log(action)
            localStorage.setItem('accessToken', action.payload.access_token)
            localStorage.setItem('refreshToken', action.payload.refresh_token)
            const obj = {
                id: action.payload.id,
                username: action.payload.username,
                email: action.payload.email
            }
            state.user = obj
            state.isAuthenticated = true
        })
    }
})

export default authSlice.reducer
export const { } = authSlice.actions