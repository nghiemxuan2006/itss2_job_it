import Filter from '../../components/filter/filter';
import BodyIndex from '../../components/layouts/body_index';
import ResultSearch from '../../components/layouts/result_search';
import { useAppSelector } from '../../app/hook';
import CardJob from '../../components/card/CardJob';

const Home = () => {
    const isClickedBtnSearch = useAppSelector(state => state.btnSearch.isClickedBtnSearch);
    const data = useAppSelector((state) => state.searchJobList.data)
    const isSendCVSuccess = useAppSelector(state => state.btnSearch.isSendCVSuccess)
    return (
        <div className='flex flex-col h-full'>
            <Filter />
            {isClickedBtnSearch || data.length || !isSendCVSuccess ? <ResultSearch /> : <BodyIndex />}
        </div>
    );
};

export default Home;
