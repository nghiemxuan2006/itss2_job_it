import { faBookmark, faCompass, faLocationDot } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './detail_company.css'; // Import your CSS file
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import CardJob from '../../components/card/CardJob';
import Money from '../../assets/money.svg';
import Bank from '../../assets/bank.svg';
interface Company {
    company_id: Number;
    name: string;
    image: string;
    address: string;
    type: string;
    scale: string;
    nation: string;
    time_work: string;
    is_overtime: boolean;
    introduction: string;
    website: string;
    fanpage: string;
    distance: number;
    jobs: [
        {
            id: number;
            name: string;
            work_location: string;
            salary: number;
            skill_requirements: string[];
        },
    ];
}

const DetailCompany = () => {
    const { id } = useParams();
    const [data, setData] = useState<Company>();

    useEffect(() => {
        const fetchTodos = async () => {
            try {
                const response = await axios.get<Company>(
                    `https://shiba-internship.azurewebsites.net/api/v1/company/${id}`,
                    {
                        headers: {
                            Authorization:
                                'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJEdWNIdXkiLCJlbWFpbCI6Imh1eUBnbWFpbC5jb20iLCJhdmF0YXIiOiJodHRwczovL3l0My5nb29nbGV1c2VyY29udGVudC5jb20vLUNGVEpIVTdmRVdiN0JZRWI2Smg5Z20xRXBldHZWR1FxdG9mMFJiaC1WUVJJem5ZWUtKeENhcXZfOUhlQmNtSm1Jc3Aydk9POUpVPXM5MDAtYy1rLWMweDAwZmZmZmZmLW5vLXJqIiwiaWF0IjoxNzAyOTI5ODQwLCJleHAiOjE3MTE1Njk4NDB9.ybp-asGd14mgRbrLEmiUSg7mbrqZgSfcCz9jFveFusQ',
                        },
                    },
                );
                setData(response.data);
                console.log('data res Company', response.data);
            } catch (error) {
                console.error('Error fetching todos:', error);
            }
        };

        fetchTodos();
    }, []);

    let content = data?.jobs.map((item, index) => {
        return (
            <div className="flex flex-row mt-3 pl-10">
                <div className={`border-2 rounded-2xl bg-white border-stone-400 w-11/12`}>
                    <div>
                        <p className="text-3xl font-semibold ml-5 mt-1">{item['name']}</p>
                    </div>
                    <div className="flex flex-row pt-1 items-center">
                        <img src={data['image']} alt="" className={`rounded border border-black w-10 h-12 ml-5`} />
                        <p className="ml-3 text-xl font-medium">{item['name']}</p>
                    </div>
                    <div className="mx-5 mt-3 mb-1">
                        <p className="border border-t-black"></p>
                    </div>
                    <div className="flex flex-row pt-1 items-center ml-3" style={{ color: '#dc2626' }}>
                        <FontAwesomeIcon icon={faLocationDot} className="ml-5 w-6 h-6" />
                        <p className="text-xl pl-2">Cách {data['distance']} km</p>
                    </div>
                    <div className="flex flex-row pt-1 items-center ml-3">
                        {/* <FontAwesomeIcon icon={faBank} className="ml-5 w-6 h-6 font-thin" /> */}
                        <img src={Bank} alt="bank" className="ml-5 w-6 h-6" />
                        <p className="text-xl pl-2">{item['work_location']}</p>
                    </div>
                    <div className="flex flex-row pt-1 items-center ml-3">
                        {/* <FontAwesomeIcon icon={faMoneyBill} className="ml-5 w-6 h-6" /> */}
                        <img src={Money} alt="money" className="ml-5 w-6 h-6" />
                        <p className="text-xl pl-2">
                            {item['salary'].toLocaleString('en-US', {
                                minimumFractionDigits: 0,
                                maximumFractionDigits: 0,
                            })}{' '}
                            đ
                        </p>
                    </div>
                    <div className="mx-5 mt-2 mb-1">
                        <p className="border border-t-black"></p>
                    </div>
                    <div className="flex flex-row pt-1 items-center ml-5">
                        {item['skill_requirements'].map((item: string, index: number) => {
                            return (
                                <div className="shadow-inner shadow-slate-500 rounded-full mb-5 ml-3" key={index}>
                                    <p className="text-xl px-2 py-1">{item}</p>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    });

    return (
        <div className="">
            <div className="bg-gradient-to-r from-fromColor to-toColor flex flex-row pl-16 py-4">
                <div className="bg-slate-50 w-24 h-20 border-solid rounded-md flex justify-center mr-6">
                    <img alt="" src={data?.image} className="w-full" />
                </div>

                <div className="flex flex-col justify-between text-white py-2 ">
                    <h3 className="text-xl">{data?.name}</h3>
                    <div className="text-sm">
                        <span className="mr-8">
                            <FontAwesomeIcon icon={faLocationDot} className="pr-2" />
                            {data?.address}
                        </span>

                        <span>
                            <FontAwesomeIcon icon={faBookmark} className="pr-2" />
                            {data?.jobs.length} công việc đang Tuyển dụng
                        </span>
                    </div>
                </div>
            </div>

            <div className="pl-16 flex flex-row bg-slate-200 py-2">
                <div className="w-8/12 flex flex-col">
                    <div className="pl-10 bg-slate-50 rounded-lg py-3 shadow-xl">
                        <h3 className="text-lg font-semibold">Giới thiệu</h3>
                    </div>
                    <div className="px-10 bg-slate-50 rounded-lg py-3 shadow-xl mt-2">
                        <div className="border-b border-zinc-300">
                            <h3 className="text-lg font-semibold pb-2 ">Thông tin chung</h3>
                        </div>
                        <div className="grid grid-cols-3 gap-4 pt-2">
                            <div>
                                <p className="text-sm">Mô hình công ty</p>
                                <span className="font-medium">{data?.type}</span>
                            </div>
                            <div>
                                <p className="text-sm">Quy mô công ty</p>
                                <span className="font-medium">{data?.scale}</span>
                            </div>
                            <div>
                                <p className="text-sm">Quốc gia</p>
                                <span className="font-medium">{data?.nation}</span>
                            </div>
                            <div>
                                <p className="text-sm">Thời gian làm việc</p>
                                <span className="font-medium">{data?.time_work}</span>
                            </div>
                            <div>
                                <p className="text-sm">Làm việc ngoài giờ</p>
                                <span className="font-medium">{data?.is_overtime ? 'Có OT' : 'Không có OT'}</span>
                            </div>
                        </div>
                    </div>
                    <div className="px-10 bg-slate-50 rounded-lg py-3 shadow-xl mt-2">
                        <div className="">
                            <h3 className="text-lg font-semibold pb-2 ">Giới thiệu công ty</h3>
                        </div>
                        <div className="border-y border-zinc-300 py-2">
                        {data?.introduction}
                            {/* <p>Chuyên cung cấp các giải pháp cho các tổ chức tài chính, ngân hàng, chứng khoán</p>
                            <ul className="description-list">
                                <li className="description-list-item">
                                    {data?.name} là công ty có vốn đầu tư Nhật Bản, chuyên phát triển phần mềm chứng
                                    khoán, tài chính cho VN, Nhật Bản, Hong Kong. {data?.name} luôn hướng tới việc cung
                                    cấp những sản sản phẩm tinh tế bằng những giải pháp, công nghệ tiên tiến nhất như
                                    slogan “Smart but Simple”. Để đạt được điều đó, chúng tôi luôn cố gắng không ngừng,
                                    liên tục học hỏi, liên tục có những chương trình đào tạo cho tất cả cán bộ công ty
                                    để đi nhanh hơn so với thị trường. Ngoài chuyên môn, việc học những kĩ năng như nâng
                                    cao khả năng tiếp cận, kĩ năng quản lý công việc, kĩ năng học tập tối ưu, phương
                                    pháp giải tỏa stress, nâng cao sức khỏe, thiền cho kĩ sư,…đã mang lại những giá trị
                                    không chỉ trong công việc
                                </li>
                                <li className="description-list-item">Quy trình quản lý chất lượng ISO 9001:2015</li>
                                <li className="description-list-item">
                                    Quy trình quản lý an toàn thông tin ISO 27001:2015
                                </li>
                            </ul>
                        </div>
                      */}
                         <div className="flex flex-row">
                            <a href={data?.website} className="text-sky-500 pt-2 px-16">
                                Website Công ty{' '}
                            </a>
                            <a  href={data?.fanpage} className="text-sky-500 pt-2 px-16">
                                Fanpage Công ty
                            </a>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="w-4/12 h-100 ">
                    <div className="flex justify-center">
                        <p className="ms-5 text-2xl font-semibold ml-5">
                            {data?.jobs.length} công việc đang Tuyển dụng
                        </p>
                    </div>
                    {/* <div className='overflow-auto h-[560px]'>
                        {content}
                    </div> */}
                    <div className="right-scrollbar">
                        <div className="outer-container cursor-pointer">{content}</div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DetailCompany;
